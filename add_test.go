package add

import (
	"testing"
)

type addTest struct {
	a, b, result int
}

var addTests = []addTest{
	{1, 2, 3},
	{2, 3, 5},
	{100, 100, 200},
}

func TestAdd(t *testing.T) {
	for _, at := range addTests {
		res := Add(at.a, at.b)
		if res != at.result {
			t.Errorf("%d + %d = %d, want %d", at.a, at.b, res, at.result)
		}
	}
}
